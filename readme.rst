unibuild
========
The universal **build** script system.

Easy install
============

.. code-block:: shell

	curl -s https://gitlab.com/sulinos/devel/unibuild/-/raw/master/netinst.sh | bash -s --

for install
===========
DESTDIR=*your_DESTDIR* bash install.sh

Simple usage
============
[TARGET=*your_target*] **unibuild** [*file/url*]

Documentation
=============
Visit https://gitlab.com/sulinos/devel/unibuild/-/blob/master/doc/building.rst
